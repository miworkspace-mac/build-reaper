#!/bin/bash

NEWLOC=`curl -L "https://www.reaper.fm/download.php" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36' 2>/dev/null | grep 'x86_64.dmg' | awk -F '"' '{print $2}' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "https://www.reaper.fm/${NEWLOC}"
fi
