#!/bin/bash

# Install re-wire plugin

if [ ! -f /Library/Application\ Support/Propellerhead\ Software/ReWire/ReWire.bundle ]; then
	
	mkdir -p /Library/Application\ Support/Propellerhead\ Software/ReWire
	
	cp  -R /Applications/REAPER.app/Contents/Plugins/ReWire.bundle /Library/Application\ Support/Propellerhead\ Software/ReWire/ReWire.bundle
	
	exit 0
	
else
	rm -rf /Library/Application\ Support/Propellerhead\ Software/ReWire/ReWire.bundle
	
	cp -R /Applications/REAPER.app/Contents/Plugins/ReWire.bundle /Library/Application\ Support/Propellerhead\ Software/ReWire/ReWire.bundle
	
	exit 0
fi